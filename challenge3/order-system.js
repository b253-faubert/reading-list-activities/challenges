class Product {
	constructor(id, name, quantity, price) {
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.price = price;
	}
}

class Order {
	constructor(products = []) {
		this.products = products
	}
}

class OrderSystem {
	constructor(orders = []) {
		this.orders = orders;
	}

	addOrder(order) {
		this.orders.push(order);
	}

	showAllOrders() {
		this.orders.map((order) => {
			console.log(order)
		})
	}

	removeProductFromOrder(product, order) {
		let foundOrder;
		this.orders.forEach((ord) {
			if (order === ord) {
				foundOrder = ord;
				return;
			}
		})
		
		foundOrder.filter((prod) => {
			return prod !== prod
		})
	}

	computeTotalOfOrder(order) {
		let total = 0;
		let ordertoCalculate;
		orders.forEach((ord) => {
			if (order === ord) {
				ordertoCalculate = ord;
			}
		})

		order.map((product) {
			total += product.quantity * product.price;
		})

		return total;
	}
}