
class Character {
	constructor(name, classType, level) {
		this.name = name;
		this.classType = classType;
		this.level = level;
		this.bag = []
	}

	dodge() {
		return "The attack missed"
	}

	faint() {
		return `${this.name} has fainted.`
	}

	attack(target) {
		return `${this.name} has attacked ${target.name}`
	}
}

let monster = new Character('monster1', "Monsters", 12)
Monster.takeDamage = function(damage) {
	return `${this.name} has taken ${damage} damage.`
}

let merchant = new Character('merchant1', "Merchant", 10)
merchant.buy = function(item) {
		this.items.push(item)
	}
merchant.sell = function(item) {
		this.items.filter((bagItem) => {
			return bagItem !== item
		})
}


let merlin = new Character();
